# Slope of a Tangent Line

The fundamental concepts of calculus are formed by reliably determining the slope of a line at a given point.

The slope of a line is its rate of change, calculated as \\( \frac{\Delta y}{\Delta x} \\) (the change in "y" divided by the change in "x"), which is a shorthand way of writing \\( \frac{y_2 - y_1}{x_2 - x_1} \\). This formula determines the change between two points on a line.

The number underneath each of the letters indicates which point it originates from - for example, \\( y_2 \\) is the y-coordinate of the second point, and \\( x_1 \\) is the x coordinate of the first point.

This definition of slope is useful in basic algebra, where a line has the same slope no matter which two points you pick. For example, for the line \\( y = x \\), the change between point \\( (1, 1) \\) and point \\( (5, 5) \\) can be determined through the slope formula:

\\[ \frac{y_2 - y_1}{x_2 - x_1} = \frac{5 - 1}{5 - 1} = \frac{4}{4} = 1 \\]

This is true for any two points on the line \\( y = x \\). Another example, with points \\( (6, 6) \\) and \\( (543, 543) \\):

\\[ \frac{y_2 - y_1}{x_2 - x_1} = \frac{543 - 6}{543 - 6} = \frac{537}{537} = 1 \\]

The line graphed from \\( y = x \\) is said to have a constant slope; for any two points on that line, the rate of change between them is 1.

Not all lines have constant slopes. Take \\( y = x^2 \\) for example. Let's find the slope between \\( (2, 4) \\) and \\( (8, 64) \\):

\\[ \frac{y_2 - y_1}{x_2 - x_1} = \frac{64 - 4}{8 - 2} = \frac{60}{6} = 10 \\]

And now let's take the slope between \\( (8, 64) \\) and \\( (16, 256) \\):

\\[ \frac{y_2 - y_1}{x_2 - x_1} = \frac{256 - 64}{16 - 8} = \frac{192}{8} = 24 \\]

Two different slopes for two different sets of points on the line.

The question we should ask ourselves: **is there a way to determine the slope of a line at a given point, even if the line doesn't have a constant slope?**

## Secant Lines

To visualize what I am about to describe, draw a rough sketch of \\( y = x^2 \\).

Place a dot on the points \\( (1, 1) \\) and \\( (3, 9) \\).

Then, draw a straight line that connects these two points.

This line that you've just drawn is called a *secant line*. A secant line is a straight line with a constant slope. When you take the slope of a secant line, you find the rate of change between the two points that the secant line intersects.


## Tangent Lines

A *tangent line* is a line which intersects another line at exactly one point.

Imagine that the two points that you've drawn on the \\( y = x^2 \\) line are moving closer together along the line (i.e. the lower point is beginning to move up the curve and the upper point is beginning to descend the curve).

What happens to the secant line as these two points move closer? Its elevation will decrease, until the two points come so close together that they are at the same coordinate. At that point, the secant line will intersect that point on the graph, and no other points. The secant line has just become a tangent line.

If the slope of a secant line is the rate of change between the two points it intersects, then what does the slope of a tangent line mean, given that it only intersects one point?

**The slope of a tangent line represents the slope of the main line at the point which the tangent line intersects**.

## Calculating the Slope of a Tangent Line

Slopes of tangent lines are theoretically the same as the slope of any line, and follows the formula \\( \frac{y_2 - y_1}{x_2 - x_1} \\).

However, we need to account for the fact that we only have one point on this line, not two. Since we only have one point, we can't calculate the slope of a tangent line without changing how we use this formula.

A good rule of thumb in these scenarios is to expand the formula to reveal any hidden assumptions we're making about it, and to reveal any additional ways we can make use of the formula.

The first step we should take is to expand y into f(x). The key difference between y and f(x) is that f(x) reminds us that y is a function of x (in other words, y is calculated by performing a mathematical operation on x).

\\[ \frac{y_2 - y_1}{x_2 - x_1} = \frac{f(x)_2 - f(x)_1}{x_2 - x_1} \\]

Now let's ask ourselves: what are \\( f(x)_2 \\) and \\( f(x)_1 \\), knowing that f(x) is calculated by performing a mathematical operation on x?

Going back to our points example, lets imagine two points \\( (x, f(x)) \\) and \\( (x_2, f(x)_2) \\). We can rewrite the second point as \\( (x_2, f(x_2)) \\).

What is \\( x_2 \\)? Since \\( x \\) and \\( x_2 \\) are on the same line, we can say that \\( x_2 \\) is the same thing as \\( x \\) plus a number. That number can be defined as \\( \Delta x \\), which is the same thing as \\( x_2 - x \\), since adding \\( x \\) and \\( \Delta x \\) would give us \\( x_2 \\):

\\[ x_2 - x = \Delta x \\]
\\[ x + \Delta x = x + (x_2 - x) = x_2 \\]

Knowing this, we can re-write the second point as \\( (x + \Delta x, f(x + \Delta x)) \\).

Now that we have two points, both being written in terms of \\( x \\), we can revise our slope formula:

\\[ \frac{f(x)_2 - f(x)_1}{x_2 - x_1} = \frac{f(x + \Delta x) - f(x)}{(x + \Delta x) - x} \\]

The denominator of our slope expression can be simplified even further:

\\[\frac{f(x + \Delta x) - f(x)}{(x + \Delta x) - x} = \frac{f(x + \Delta x) - f(x)}{\Delta x}\\]

This new formula - \\( \frac{f(x + \Delta x) - f(x)}{\Delta x} \\) - is the most expanded form of the slope formula we can get. All of the assumptions we had been making in the simplest form of the formula have been removed, and everything has been painstakingly defined.

How do we plug the tangent line into this formula? We can try this:

\\[ \frac{f(x + \Delta x) - f(x)}{\Delta x} = \frac{f(x + 0) - f(x)}{0} = \frac{f(x) - f(x)}{0} = \frac{0}{0} \\]

But that doesn't work! Dividing by zero is a mathematically impossible operation; since division is about dividing a number into groups, how do we divide a number into zero groups? Dividing a number into one group is a doable task, but dividing a number into zero groups doesn't make sense.

This issue arises because the change between our two points is zero - there is no change at all. We can fix that by actually using a second point. For example, let's say we want the slope of the tangent line at \\( x = 2 \\), or \\( (2, 4) \\) since our equation is \\( f(x) = x^2 \\):

\\[ \frac{f(x + \Delta x) - f(x)}{\Delta x} = \frac{f(2.0001) - f(1.9999)}{2.0001 - 1.9999} = \frac{4.00040001 - 3.99960001}{2.0001 - 1.9999} = 3.9999999999... \\]

We used two points in the above example: \\( (1.9999, 3.99960001) \\) and \\( (2.0001, 4.00040001) \\). These two points are extremely close to our tangent-line point of \\( (2, 4) \\).

From our calculations we can see that the slope of the secant line that is very close to (but not quite) at \\( x = 2 \\) is equal to 3.9 repeating, or approximately 4. From this we can *infer* that the slope of the tangent line at \\( x = 2 \\) is 4.

**But we do not know that for certain!**

Equations more complex than this one may warrant creating two points even closer to the target point - so close that the average calculator won't be able to spot the difference between those two points and the tangent line, and as a result will bring us back to the nonsensical \\( \frac{0}{0} \\) answer.

## Precisely Calculating the Slope of a Tangent Line

We know that using two points that approach the tangent line can allow us to approximate the slope of the tangent line. Let's start from there.

Two points: \\( (x, f(x)) \\) and \\( (x + \Delta x, f(x + \Delta x)) \\).

The closer together they are, the closer they resemble a single point - and the more their math is applicable to a single point.

Points that are closer together have hardly any distance between them - so \\( \Delta x \\) should be very small.

We can say that \\( \Delta x \\) is so small, in fact, that it *approaches zero*.

When we say that a variable in an equation is approaching a value, we can use *limits* to express that fact:

\\[ \lim_{\Delta x \to 0} \frac{f(x + \Delta x) - f(x)}{\Delta x} \\]

Let's substitute \\( f(x) \\) for \\( x^2 \\):

\\[ \lim_{\Delta x \to 0} \frac{f(x + \Delta x) - f(x)}{\Delta x} = \lim_{\Delta x \to 0} \frac{(x + \Delta x)^2 - x^2}{\Delta x} = \lim_{\Delta x \to 0} \frac{x^2 + 2x \Delta x + \Delta x^2 - x^2}{\Delta x} = \lim_{\Delta x \to 0} \frac{2x \Delta x + \Delta x^2}{\Delta x} \\]

Now we can factor out \\( \Delta x \\):

\\[ \lim_{\Delta x \to 0} \frac{2x \Delta x + \Delta x^2}{\Delta x} = \lim_{\Delta x \to 0} \frac{\Delta x (2x + \Delta x)}{\Delta x} = \lim_{\Delta x \to 0} (2x + \Delta x) \\]

At last, now that we are no longer in danger of dividing by zero (since we eliminated the denominator), we can substitute 0 for \\( \Delta x \\):

\\[ \lim_{\Delta x \to 0} (2x + \Delta x) = 2x + 0 = 2x \\]

**The slope of a line which is tangent to** \\( f(x) = x^2 \\) **, for any x, is** \\( 2x \\).

Earlier we wanted the slope of the tangent line for \\(x = 2 \\):

\\[ 2x = 2(2) = 4 \\]

**The equation for the slope of the tangent line for any x is the same as the equation for the slope of the main line at any given x.** You can now find the slope of \\( y = x^2 \\), or any other line, at any point that you so choose.
