# Calculus

Calculus is the study of continuous change.

The ideas and core concepts of calculus are founded primarily on Algebra and Trigonometry. These will be touched upon slightly within this section, but I recommend developing a more robust understanding of these subjects before beginning to study calculus.
