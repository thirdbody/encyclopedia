# Milo's Encyclopedia

A compendium of knowledge that I knew about and decided to write about.

## Usage

Take a look at https://encyclopedia.milogilad.com.

## Philosophy

This encyclopedia is founded and written using a comprehensive form of the Feynman technique, the principles of which are:

1) Begin at the top-most level of a subject and move down to the simplest concepts within (i.e. Calculus -> slope of a tangent line -> tangent line -> slope).
2) Fully explain each concept.
3) Provide a summary/recap at the end of each concept's explanation, if it can be simplified.

## Subjects Covered

At present, the encyclopedia is blank. I plan for the first section to be about single-variable calculus.
