#! /usr/bin/env bash

rm -rf public
mkdir public
cd mdbook
for d in */; do
    cd "$d"
    mdbook build
    mv ./book ../../public/"${d%/}"
    cd ..
done

cd ../public
mv ./home/* .
rm -r ./home
